from debian:stable

LABEL maintainer "Jean-Francois Rey <jean-francois.rey@inra.fr>"

RUN apt-get update \
  && apt-get install -y jags g++ pkg-config git autotools-dev autoconf libtool make 
RUN git clone https://gitlab.paca.inra.fr/jfrey/jags-module.git
RUN cd jags-module/mecastat \
  && autoreconf -ifv; exit 0
RUN cd jags-module/mecastat \
  && ./configure \
  && make \
  && make install 

RUN apt-get remove g++ git pkg-config autotools autoconf libtool make --purge -y \
  && apt-get autoremove -y \
  && apt-get clean \
  && rm -rf /tmp/downloaded_packages/ /tmp/*.rds \
  && rm -rf /var/lib/apt/lists/*

RUN useradd -ms /bin/bash jags
RUN cd /jags-module/ && cp -r tests/* /home/jags/ \
  && rm -rf /jags-module
VOLUME /home/jags/
ENV LD_LIBRARY_PATH=/usr/local/lib/JAGS/modules-4/:$LD_LIBRARY_PATH 
WORKDIR /home/jags/

USER jags

CMD ["/usr/bin/jags","script.jags"]

