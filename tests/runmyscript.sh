#!/bin/sh
# runmyscript.sh input.txt output.txt
# add or change command or script to run
# input file "input.txt", each parameters are separated by a space
# output file name "output.txt"
# -> nbofrow nbofcolumns nbofmatrix data...
# -> data order : matrix1 first_columns_values second_columns_values matrix2 ...

#pwd > iamhere.txt

if [ $# -lt 2 ]; then
  echo "ERROR in number of runmyscript.sh parameters !"
  exit 1
else
  echo "2 2 2\n11 13 12 14 21 23 22 24\n" > $2 
fi

exit 0
