# REAMDE for testing JAGS MecaStat module 

This directory contains a simple model with fake data and results.
To more explanation see [../README.md](../README.md).

Files : 
* data.txt : jags data for the model
* init.txt : variables initialization
* model.txt : the jags model
* script.jags : the jags script to be run
* dim.txt : a file containing the dimensions of the data of the runmyscript.sh output contained in output.txt
* parallel.R : a test file using rjags and running a parallel test. 
* runmyscript.sh/.bat : script called by the module on Linux or Windows.

## Linux

Run jags script
```shell
jags script.jags
```

Run parallel.R script
```shell
Rscript parallel.R
```

## Windows

Launch jags and to copy and paste code in _script.jags_.  

Or try parallel.R in R.  


Run jags script 
```shell
set LTDL_LIBRARY_PATH=C:\JAGS\JAGS-4.2.0\x64\modules\
set PATH=C:\JAGS\JAGS-4.2.0\x64\bin\;%PATH%
jags-terminal.exe script.jags
```

## Using Docker

```
docker run gitlab.paca.inra.fr:4567/jfrey/jags-module
# to change working directory with your script (the jags script name is script.jags)
docker run -v /my/own/path:/home/jags gitlab.paca.inra.fr:4567/jfrey/jags-module
```
