rem # runmyscript.sh input.txt output.txt
rem # add or change command or script to run
rem # input file "input.txt", each parameters are separated by a space
rem # output file name "output.txt"
rem # -> nbofrow nbofcolumns nbofmatrix data...
rem # -> data order : matrix1 first_columns_values second_columns_values matrix2 ...

@echo off
rem echo %1 %2

if [%2]==[] (
  echo 'ERROR in number of runmyscript.bat parameters'
  goto end
)

echo 2 2 2 > %2
echo 11 13 12 14 21 23 22 24 >> %2
rem launch a new cmd with parameters
rem rem start cmd %1 %2
rem or
rem newscript.bat %1 %2

:end
