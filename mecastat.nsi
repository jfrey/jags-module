; MecaStat JAGS Module nsi script 
; Author Jean-François Rey
; Version 0.3
; Date 01/02/2017

!include "MUI2.nsh"
!include sections.nsh

!define VERSION "0.4"
!define MODULE "MecaStat"
;!define ARCH "x64"
;!define ARCH "i386"
Var ARCH
!define JAGSVERSION "4.3.0"

Var INSTDATADIR

Name "MecaStat JAGS Module"
OutFile "${MODULE}-${VERSION}-${ARCH}.exe"
InstallDir "$PROGRAMFILES\JAGS-${JAGSVERSION}\${ARCH}\modules"

AllowRootDirInstall true
RequestExecutionLevel user

;-------- Pages ---------------------------------------------------------------
; Pages Install
!insertmacro MUI_PAGE_WELCOME
Page custom CustomWelcomePage
!InsertMacro MUI_PAGE_LICENSE mecastat\LICENSE
!insertmacro MUI_PAGE_COMPONENTS
!define MUI_PAGE_CUSTOMFUNCTION_PRE CustomDirectoryPage1
!define MUI_DIRECTORYPAGE_TEXT_DESTINATION "Module Destination Folder"
!insertmacro MUI_PAGE_DIRECTORY
!define MUI_PAGE_CUSTOMFUNCTION_PRE CustomDirectoryPage2
!define MUI_DIRECTORYPAGE_VARIABLE $INSTDATADIR
!define MUI_DIRECTORYPAGE_TEXT_TOP "Setup will install ${MODULE} Module Configurations Files in the following folder. To install in a different folder, click Browse and select another folder. \r\n\r!!! Please select the modules directory where JAGS is installed !!!\r\n\r Click Install to start the installation."
!define MUI_DIRECTORYPAGE_TEXT_DESTINATION "Configurations Files Destination Folder"
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

; Pages Uninstall
!insertmacro MUI_UNPAGE_WELCOME
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH


;-------- Language ------------------------------------------------------------
!insertmacro MUI_LANGUAGE "English"

;-------- Sections ------------------------------------------------------------

  ; module files (libraries)
Section "Module Files" Section1
SetOutPath $INSTDIR
FILE "mecastat\src\.libs\MecaStat.dll"
FILE "mecastat\src\.libs\MecaStat.dll.a"
FILE "mecastat\src\.libs\MecaStat.la"
FILE "mecastat\LICENSE"

;WriteRegStr HKCU "${MODULE}-${ARCH}" "" $INSTDIR

WriteUninstaller "$INSTDIR\Uninstall-${MODULE}-${VERSION}-${ARCH}.exe"

SectionEnd

  ; module configuration files
Section "Basic config files" Section2
SetOutPath $INSTDATADIR
File "mecastat\data\runmyscript.bat"
File "mecastat\data\dim.txt"
SectionEnd

  ; Section 1 and 2 descriptions
LangString DESC_Section1 ${LANG_ENGLISH} "Module Libraries."
LangString DESC_Section2 ${LANG_ENGLISH} "Module Configuration Files."
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
	!insertmacro MUI_DESCRIPTION_TEXT ${Section1} $(DESC_Section1)
	!insertmacro MUI_DESCRIPTION_TEXT ${Section2} $(DESC_Section2)
!insertmacro MUI_FUNCTION_DESCRIPTION_END

  ; Uninstall section
Section "Uninstall"

  Delete "$INSTDIR\MecaStat.dll"
  Delete "$INSTDIR\MecaStat.dll.a"
  Delete "$INSTDIR\MecaStat.la"
  Delete "$INSTDIR\LICENSE"

  Delete "$INSTDIR\Uninstall-${MODULE}-${VERSION}-${ARCH}.exe"
SectionEnd

;-------- Custom Pages --------------------------------------------------------

;-------- Functions -----------------------------------------------------------

  ; Show directory page if section selected
Function CustomDirectoryPage1
${IfNot} ${SectionIsSelected} ${Section1}
Abort
${EndIf}  
FunctionEnd

Function CustomDirectoryPage2
${IfNot} ${SectionIsSelected} ${Section2}
Abort
${EndIf}  
FunctionEnd

Var TEXT

  ; Welcome page function
Function CustomWelcomePage
  !insertmacro MUI_HEADER_TEXT "MecaStat JAGS Module Informations" "MecaStat JAGS Module details"
  nsDialogs::create 1018
  ${NSD_CreateLabel} 0 0 100% 12u "Module Informations"

  nsDialogs::CreateControl STATIC "${WS_VISIBLE}|${WS_CHILD}|${WS_CLIPSIBLINGS}" \
0 0 13u 100% 100% \
"MecaStat JAGS Module is developer by the BioSP Unit of INRA. It allow to use JAGS with Mecanico-Statistic approaches.$\r$\nFor more information send email to : jean-francois.rey@inra.fr .$\r$\nWeb site : http://biosp.org/MecaStat "
Pop $TEXT

setCtlColors $TEXT "" 0xffffff

  nsDialogs::Show
FunctionEnd

  ; Init function
Function .onInit
  StrCpy $INSTDATADIR "$DOCUMENTS\MecaStat"
${If} $ARCH == ""
  StrCpy $ARCH "i386"
${EndIf}
FunctionEnd

