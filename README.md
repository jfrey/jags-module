# MecaStat JAGS MODULE

[![pipeline status](https://gitlab.paca.inra.fr/jfrey/jags-module/badges/master/pipeline.svg)](https://gitlab.paca.inra.fr/jfrey/jags-module/commits/master)  


## description

The MecaStat JAGS module integrate mechanico-statical approaches (EDP, EDO) in epidemilogy.  
It mainly allow to call a shell script from JAGS.

## Install from binary

* [Windows](binary/Windows/) | use artifact from CI pipeline

## Install from source

* [Windows INSTALL](mecastat/README.WIN.md)
* [Linux / MACOS INSTALL](mecastat/README.md)

# Use docker image

```
# run a demo from test directory
docker run gitlab.paca.inra.fr:4567/jfrey/jags-module
# to run your own script contain in /my/own/path/
# with a jags script call script.jags
docker run -v /my/own/path:/home/jags gitlab.paca.inra.fr:4567/jfrey/jags-module
```

## How to compile (Developper mode)

> You need to install package "autotools", "autoconf", "automake", "libtool-bin", "pkgconfig", "g++", "make".

```shell
cd mecastat
autoreconf -ifv
./configure
make
sudo make install
```
> note : add _--prefix=/dir/to/localinstall/_ to configure for a local install  
> note : autoreconf is for developper mode (create configure et makefiles)  
> note : at run time add export LD_LIBRARY_PATH=path_to_jags/jags/lib/ to indicate where ljags is.  
> example : ```./configure --prefix=/usr/local/ CXXFLAGS=/usr/local/include/ LDFLAGS=/usr/local/lib/ LIBS=-lflags```

To remove install :  
```shell
sudo make uninstall
```

## How to distribute

Create a tar.gz  

```shell
make dist
```
## How to use module MecaStat

### to start

> see [tests directory](tests/README.md)

* create a script named __runmyscript.sh__, it will be call by the module
* write your things into __runmyscript.sh__
* create a file named dim.txt and add the dimensions size expected for your output file (ex: nbofrow nbofcols nbofmatrix). Max 3Dimension.
* run jags
* `load MecaStat`
* to call your script use `dynamic` function (ex : `lambda <- dynamic(alpha,beta)` )

### input and output runmyscript.sh format (dynamic function)

The `dynamic` function write parameters into the __XXX_input.txt__ file, run __runmyscript.sh__ and read __XXX_output.txt__ file. input and output file name have to be in runmyscript.sh parameters.

* __XXX_input.txt__ : parameters list separated by a space
* __XXX_output.txt__ : the three first values are the data size _nbofrow_ _nbofcolumns_ _nbofmatrix_ follow by matrix values recorded by columns. If values are of 1 or 2 dimensions write the size to _1_ (ex: _nbofrow_ _1_ _1_).

```
matrix 1      matrix 2
11 12            21 22 
13 14            23 24
```
```
output.txt file :

2 2 2 11 13 12 14 21 23 22 24

```

## Bugs

### rjags can't find module

```
# the variable jags.moddir should be set to the MecaStat module path
getOption("jags.moddir")
```

## Authors
Jean-Francois Rey  
Julien Papaix  
