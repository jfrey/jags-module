/*******************************************************************************
MecaStat Jags module 
Copyright (C) <2016>  Jean-François Rey <jean-francois.rey@inra.fr>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/


#ifndef __FUNC_EXECUTESYSTEMCMD_H__
#define __FUNC_EXECUTESYSTEMCMD_H__


#include <config.h>
#include <util/dim.h>
#include <function/ArrayFunction.h>
#include <string>
#include <fstream>
#include <iostream>
#include <istream>
#include <vector>
#include <sys/types.h>
#include <unistd.h>
#if defined(__WIN32__) || defined(__WIN64__)
#define HAVE_STRUCT_TIMESPEC
#include <windows.h>
#include <shellapi.h>
#endif
#include <pthread.h>
#include <sstream>

// Default path for template file
#define DEFAULT_PATH "./"

// File name of the system call input file
#define INPUT_FILE "input.txt"

// File name of the system call output file
#define OUTPUT_FILE "output.txt"


namespace jags {
namespace MecaStat {

  /* Class ExecuteSystemCmd
   * Allow to run a system call
   * Evaluation return a 3D dimension array
   *
   */
  class ExecuteSystemCmd : public ArrayFunction {
    public:
      ExecuteSystemCmd(std::string const &name,unsigned int npar);
      virtual void evaluate(double *value,
                           std::vector<double const *> const &args,
                           std::vector<std::vector<unsigned int> > const &dims) const;
      virtual bool checkParameterDim(std::vector<std::vector<unsigned int> > const &dims) const;


      virtual std::vector<unsigned int> dim(std::vector <std::vector<unsigned int> > const &dims,
                                            std::vector <double const *> const &values) const;

  };

}
}

#endif
