/*******************************************************************************
MecaStat Jags module 
Copyright (C) <2016>  Jean-François Rey <jean-francois.rey@inra.fr>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#include "ExecuteSystemCmd.h"

namespace jags {
namespace MecaStat {

    ExecuteSystemCmd::ExecuteSystemCmd(std::string const &name,unsigned int npar) : ArrayFunction(name,npar){}

    // Note : dims[0][0] == nbrow, dims[0][1] == ncols if dims[0].size==2, dims[0][2] == 3D if dims[0] == 3 
    // !!! DO NOT allocate value !!!
    // value is fill by columns
    //
    // Evaluation fonction
    // params value the return value
    // param args function parameters
    // param dims args paramters dimensions
    void ExecuteSystemCmd::evaluate(double *value, std::vector<double const *> const &args, std::vector<std::vector<unsigned int> > const &dims) const
    {

      //std::cerr<<"Tid "<<pthread_self() <<" Pid "<<getpid()<<" PPid "<<getppid()<<" Value count "<<count++<<std::endl;
      int ret = 1;
      char * temp;
      
      /*
      std::cerr<< "Evaluation "<<args.size()<<std::endl;
      for (unsigned int i = 0; i < args.size(); ++i) {
        std::cerr<<*args[i]<<" size: "<<(dims[i]).size();
        std::cerr<<std::endl;
      }

      std::cerr<<std::endl;
     */ 
      
      // ## get default path + thread ID + Process ID + input/output file name

      // get pathname of the current working directory
      char buf[1024];
      temp = getcwd(buf,1024);
      std::string path(buf);
      //std::string path(getenv("PWD"));

      std::stringstream sprefix;
#if defined(__WIN32__) || defined(__WIN64__)
      sprefix << path << "/" << getpid() << "_" << GetCurrentThreadId() << "_";
#else
      sprefix << path << "/" << getpid() << "_" << pthread_self() << "_";
#endif
      
      std::stringstream input_file;
      input_file << sprefix.str() << INPUT_FILE;
      
      std::stringstream output_file;
      output_file << sprefix.str() << OUTPUT_FILE;

      // write parameters in INPUT_FILE
      std::ofstream output;
      output.open(input_file.str().c_str());
      if( output.is_open())
      {
        for(unsigned int i=0; i<args.size(); ++i) 
          for(unsigned int j=0; j<dims[i].size(); ++j)
            output << args[i][j] << " ";
      }
      else {
        std::cerr << "ERROR : can't open " << input_file.str() << " file." << std::endl;
      }
      output.close();

      //system("Rscript --vanilla --no-save --no-restore /home/jp/Documents/travail/Documents/modelisation/Cakile/modele/EXjags/MODdyn.r input.txt output.txt");
      // Call script or whatever with INPUT_FILE as input file and OUTPUT_FILE as output file
      //ret = system("pwd > oujesuis.txt ");
      //
#if defined(__WIN32__) || defined(__WIN64__)
      //ret = system( ("" + path + '\\' + "runmyscript.bat \"" + input_file.str() + "\" \"" + output_file.str() + "\"").c_str());
      // structure for ShellExecuteEx information
      SHELLEXECUTEINFO lpExec;
      lpExec.cbSize = sizeof(SHELLEXECUTEINFO);
      lpExec.hwnd = NULL;
      lpExec.lpFile = (path + "/runmyscript.bat").c_str();
      lpExec.fMask = SEE_MASK_NOCLOSEPROCESS;
      lpExec.lpVerb = "open";
      lpExec.lpParameters = ("\"" + input_file.str() + "\" \"" + output_file.str() + "\"").c_str();
      lpExec.lpDirectory = NULL;
      lpExec.nShow = 0;
      lpExec.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;

      // launch script
      HRESULT hres = ShellExecuteEx(&lpExec);

      // Check result and wait shell call endind
      if( hres && lpExec.hProcess != NULL)
      {
        WaitForSingleObject(lpExec.hProcess, INFINITE);
        CloseHandle(lpExec.hProcess);
      }
      else {
        std::cerr << "WARNING : maybe command call to runmyscript.bat or the script himself fail : code " << GetLastError() << std::endl;
      }

#else
      ret = system( (path + '/' + "runmyscript.sh \"" + input_file.str() + "\" \"" + output_file.str() + "\"").c_str());
      if(ret) { std::cerr << "WARNING : maybe command in system call fail : code "<< ret << std::endl;}
#endif


      // Read result of previous system call
      unsigned int r,c,m;   // nb of row, columns and matrix
      std::ifstream inputf(output_file.str().c_str());
      if( inputf.is_open()) 
      {
        inputf >> r >> c >> m;
        // fill value by columns       
        for(unsigned int k=0 ; k<m ; ++k)
          for(unsigned int j=0 ; j<c ; ++j)
            for(unsigned int i=0 ; i<r ; ++i)
              inputf >> value[k*(r*c) + r*j + i];
      }
      else {
        std::cerr << "ERROR : can't open " << output_file.str() << " file." << std::endl;
      }
      inputf.close();
      //std::cerr << value[0*(r*c) + r*0 + 13] << std::endl;
      //std::cerr << value[1*(r*c) + r*0 + 13];

    }

    // Check parameters dimension 
    bool ExecuteSystemCmd::checkParameterDim(std::vector<std::vector<unsigned int> > const &dims) const
    {
      //return( isScalar(dims[0]) || isVector(dims[0]) || isMatrix(dims[0]); 
      return true;
     
    }

     // param dims parameters dimension
     // param values pointer to parameters value
     // return the dimensions of the value return by evaluation,
     // in our case 3 dimensions : 
     // - first rows size
     // - second columns size
     // - third number of matrix
     std::vector<unsigned int> ExecuteSystemCmd::dim(std::vector <std::vector<unsigned int> > const &dims, std::vector <double const *> const &values) const
     {
       //std::cerr<<"Tid "<<pthread_self() <<"Pid "<<getpid()<<" PPid "<<getppid()<<std::endl;
       unsigned int dimension;  // size of a dimension
       std::vector<unsigned int> res;


       // read dimension from OUTPUT_FILE
       std::ifstream inputf("dim.txt");
       if( inputf.is_open())
       {
         while(inputf >> dimension) res.push_back(dimension); 
         inputf.close();
       }
       else {
         std::cerr << "ERROR : can't open dim.txt file." << std::endl;
         res.push_back(0);
       }
       return (res);
     };

}
}
