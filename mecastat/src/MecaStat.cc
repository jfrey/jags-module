/*******************************************************************************
MecaStat Jags module 
Copyright (C) <2016>  Jean-François Rey <jean-francois.rey@inra.fr>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#define _XOPEN_SOURCE
#include <module/Module.h>
#include <functions/ExecuteSystemCmd.h>

using std::vector;

namespace jags {
namespace MecaStat {

  /* Class SCModule
   * JAGS module to call system commands
   * 
   *
   */
  class MSModule : public Module {
    public:
      MSModule();
      ~MSModule();
  };

  MSModule::MSModule() : Module("MecaStat") {
    // Module get :
    // dynamic function with dynamic parameters
    // it call system
    insert(new ExecuteSystemCmd("dynamic",0));
  }

  MSModule::~MSModule() {
    std::vector<Function*> const &fvec = functions();
    for (unsigned int i = 0; i < fvec.size(); ++i) {
      delete fvec[i];
    }
  }

}
}

jags::MecaStat::MSModule _MecaStat_module;
