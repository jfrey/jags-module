# README for MecaStat JAGS Module

## DESCRIPTION
  The MecaStat JAGS module integrate mechanico-statical approaches (EDP, EDO) in epidemilogy.  
  It mainly allow to call a shell script from JAGS.  


  This module implement a function (ArrayFunction) that give parameters to a script (through a file) call by system(). The output of the script (a file) is read and send to JAGS via a double array. (Currently a double * that is define as 3 dimensions in the object parameters).

## INSTALL

* [Windows INSTALL](README.WIN.md)

### Dependencies

* JAGS (jags >= 4.0.0)  
  Available from : http://mcmc-jags.sourceforge.net/  
  JAGS is Just Another Gibbs Sampler.  It is a program for analysis of Bayesian hierarchical models using Markov Chain Monte Carlo (MCMC) simulation

* g++ (g++ >= 5.4.0)  
  C++ compiler

* pkg-config (suggested)

### How to install

```shell
tar -xvzf JAGS-MECASTAT-MODULE-x.x.tar.gz
cd JAGS-MECASTAT-MODULE-x.x
./configure
make
sudo make install
```
> note : add _--prefix=/dir/to/localinstall/_ to configure for a local install.  
> note : at run time add __export LD_LIBRARY_PATH=path_to_jags/jags/lib/modules/__ to indicate where is installed the jags modules.   
> example : ```./configure --prefix=/usr/local/ CXXFLAGS=/usr/local/include/ LDFLAGS=/usr/local/lib/ LIBS=-lflags```

### How to use module MecaStat

#### to start

* create a script named __runmyscript.sh__ (or __runmyscript.bat__ for windows), it will be call by the module
* write your things into __runmyscript.sh__
* create a file named dim.txt and add the dimensions size expected for your output file (ex: nbofrow nbofcols nbofmatrix). Max 3Dimension.
* run jags
* `load MecaStat`
* to call your script use `dynamic` function (ex : `lambda <- dynamic(alpha,beta)` )

#### input and output runmyscript.sh format (dynamic function)

The `dynamic` function write parameters into the __XXX_input.txt__ file, run __runmyscript.sh__ and read __XXX_output.txt__ file. Input and output file name have to be in runmyscript.sh parameters.

* __XXX_input.txt__ : parameters list separated by a space
* __XXX_output.txt__ : the three first values are the data size _nbofrow_ _nbofcolumns_ _nbofmatrix_ follow by matrix values recorded by columns. If values are of 1 or 2 dimensions write the size to _1_ (ex: _nbofrow_ _1_ _1_).

```
matrix 1      matrix 2
11 12            21 22 
13 14            23 24
```
```
output.txt file :

2 2 2
11 13 12 14 21 23 22 24

```

## ACKNOWLEDGEMENTS
  Many thanks to Jean-François Rey for useful help.

## KNOWN BUGS
  Many.

## COPYRIGHT
  See the COPYING file.

## AUTHORS
  Jean-Francois Rey  
  Julien Papaix  
