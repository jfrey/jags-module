# WINDOWS Cookbook for JAGS MecaStat modules 

We'll use the same tools as explained in 'JAGS Version 4.0.0 install manual' for compatibility but you can do your own recipe.

The module was compiled and tested under Windows 32 and 64 bits.

## Classic install

### Preparing the build environnement

You need to install the following packages :
* The Rtools compiler suite for windows
* MSYS
* JAGS

> Note : it's __better__ to install this tools at root path (in C:\\), to avoid space in variables path and troubles.

#### Rtools
For compatibility, we use the same version as in __JAGS install manual__ that is [__Rtools 3.3__](https://cran.r-project.org/bin/windows/Rtools/Rtools33.exe) which is based on gcc 4.6.3.  
Install directory : __c:\Rtools__ , Normal install and uncheck system update.

#### MSYS
Same as in __JAGS install manual__.
Install last version of [__MinGW__](https://sourceforge.net/projects/mingw/files/Installer/mingw-get-setup.exe/download) (Installer mingw-get-setup.exe) with only __msys-base__ selected for installation.  
Install directory : __c:\MinGW__.  
__Msys__ will be installed in __c:\MinGW\msys\1.0__ . 
Open _msys_ and edit the file _/etc/fstab_ (or edit C:\MinGW\msys\1.0\etc\fstab) and add the following line ```C:\Rtools\gcc-4.6.3\    /mingw``` , comment with a '#' the other lines.  

> Note : There is a mistake in the paper, in the file __fstab__ you should use __c:\Rtools\gcc-4.6.3__ instead of _c:\Rtools\gcc-4.6.3\bin_

#### JAGS
For compatibility, install the __JAGS version 4.0.0__ for windows including header files and 32 and/or 64 bits version (tested under JAGS-4.2.0-Rtools33 too).  
Install directory : __c:\JAGS\JAGS-4.0.0__  

### Compiling MecaStat

Copy JAGS-MECASTAT-MODULE-x.x.tar.gz into c:\MinGW\msys\0.1\home\<username>\ )  
Launch __msys__ (_c:\MinGW\msys\0.1\msys.bat_)  

Untar MecaStat package  
```shell
tar -xvzf JAGS-MECASTAT-MODULE-x.x.tar.gz
```  

Go to module directory  
```shell
cd JAGS-MECASTAT-MODULE-x.x
```

Launch compilation  

Replace JAGS PATH (_/c/JAGS/JAGS-4.0.0_) with your installation path and use __i386__ for 32 bits compilation or __x64__ for 64 bits compilation. Change _-m_ option with _-m32_ or _-m64_ for architecture compatibility.

```shell
CXXFLAGS="-I/c/JAGS/JAGS-4.0.0/include" LDFLAGS="-L/c/JAGS/JAGS-4.0.0/x64/lib/ -L/c/JAGS/JAGS-4.0.0/x64/bin/" CXX="g++ -m64" ./configure --prefix="/c/JAGS/JAGS-4.0.0/x64"
make clean
make
```

At this step the module librairies should be found in _JAGS-MECASTAT-MODULE-x.x/src/.libs/_ .

### Install MecaStat
Copy module libraries  
```shell
cp src/.libs/MecaStat.dll src/.libs/MecaStat.dll.a src/.libs/MecaStat.la /c/JAGS/JAGS-4.0.0/x64/modules/
```

__!!! ENJOY MecaStat module for JAGS !!!__

## Tests

Launch jags and load MecaStat module.
```shell
load MecaStat
```

Or launch cmd.exe  
```shell
set LTDL_LIBRARY_PATH=C:\JAGS\JAGS4.0.0\x64\modules\
set PATH=%PATH%;C:\JAGS\JAGS-4.0.0\x64\bin\
jags.bat
load MecaStat
```

msys test 
```shell
export LTDL_LIBRARY_PATH=/c/JAGS/JAGS-4.0.0/x64/modules/
export PATH=$PATH:/c/JAGS/JAGS-4.0.0/x64/bin/
jags-terminal.exe
load MecaStat
```

> Module should be loaded (print __OK__).

See _tests_ directory for testing installation.


## Others modes

### Native pthread

In native mode under Linux we use pthread for paralleling process.

To use pthread you can install a full version of MinGW (with pthread installed) and compile with MinGW.  
Or you can compile and install pthread-win32 using the same tools as in the first part.

Download [pthread-win32](https://www.sourceware.org/pthreads-win32/).  
Launch _msys_.  
Unzip and go to _pthreads.2_ directory.  
Edit _GNUmakefile_ file and change this :  
* for 32 bits : 
```
CC = $(CROSS)gcc -m32
CXX = $CROSS)g++ -m32
RC = $(CROSS)windres --target=pe-i686
```
* for 64 bits : 
```
CC = $(CROSS)gcc -m64
CXX = $CROSS)g++ -m64
RC = $(CROSS)windres --target=pe-x86-64
```
Launch compilation  
```{bash}
make GC
```

Copy libraries into compiler corresponding directory (or for simplicity into JAGS directory)  
```{bash}
cp libpthreadGC2.a /c/JAGS/JAGS-4.0.0/arch/lib/
cp pthreadGC2.dll /c/JAGS/JAGS-4.0.0/arch/bin/
cp pthread.h semaphore.h sched.h /c/JAGS/JAGS-4.0.0/include/
```

To compile MecaStat add to __LDFLAGS__ variable _"-lpthreadGC2"_.

crossfinger...

### Developer mode

* trouble with pkg-config : Download pkg-config sources
```
./configure --with-internal-glib
make
make install
cp /local/share/aclocal/pkg.m4 /share/aclocal/pkg.m4
```



